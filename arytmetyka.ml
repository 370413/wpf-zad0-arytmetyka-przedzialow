(* Autor: Tomasz Necio *)
(* Code Reviewer: Marcin Fryz *)

(* Wartość reprezentowana będzie przez przedział [l; r] z tym, że jeżeli l > r
   będzie to interpretowane jako "antyprzedział": [l; inf) U (-inf; r]. 
   Uwaga. Jeżeli do przedziału należy -inf lub inf znaczy to tylko że
   brak jest górnego/dolnego ograniczenia na wielkość, a nie że nieskończoność
   należy do niego. Dlatego np. wykluczone są przedziały puste (-inf, -inf)
   i też razy R {0} = {0}. Zbiór pusty będzie reprezentowany przez antyprzedział
   {l = inf; r = -inf}.
*)

type wartosc = {l : float; r: float} 


(* * * * FUNKCJE POMOCNICZE I STAŁE DO DZIAŁAŃ NA WARTOŚCIACH * * * *)


(* Funkcja pomocnicza rozróżniająca antyprzedział od przedziału *)
let jest_przedzialem w = w.l <= w.r

(* definicje pomocnych stałych*)
let zero = {l = 0.; r = 0.}
let rzeczywiste = {l = -.infinity; r = infinity}
let pusty = {l = infinity; r = -.infinity}
let niesk = {l = infinity; r = infinity}
let minus_niesk = {l = neg_infinity; r = neg_infinity}

(* funkcje pomocnicze do obsługi (anty)przedziałów *)
let rozbij a =
    if jest_przedzialem a then a, a else
        {l = neg_infinity; r = a.r}, {l = a.l; r = infinity}

let jest_niesk a = (a = niesk || a = minus_niesk)

let jest_pusty a = (a = pusty)

let odbij a = {l = a.r; r = a.l}

let napraw a =
    if jest_niesk a then pusty else
    (* przedział a jest pusty albo zawiera _jakieś_ liczby != +-inf *)
    if jest_pusty a || jest_przedzialem a then a 
    (* stripujemy antyprzedział z {+-inf} *)
    else let a1, a2  = rozbij a in
        if jest_niesk a1 then a2
        else if jest_niesk a2 then a1
        else a

let min_in_list l =
    let rec aux acc l =
        match l with
        | [] -> acc
        | h::t -> if h <> h then aux (min 0. acc) t else aux (min h acc) t
    in aux max_float l

let max_in_list l =
    let rec aux acc l =
        match l with
        | [] -> acc
        | h::t -> if h <> h then aux (max 0. acc) t else aux (max h acc) t
    in aux (-.max_float) l

let przecinaja_sie_przedzialy w1 w2 =
    assert (jest_przedzialem w1 && jest_przedzialem w2);
    (w2.l >= w1.l && w2.l <= w1.r) || (w2.r >= w1.l && w2.r <= w1.r)

(* funkcja suma działa tylko na wartości mogące stworzyć poprawną wartość *)
let suma w1 w2 =
    if jest_pusty w1 then w2 else if jest_pusty w2 then w1 else
    let rec aux w1 w2 =
        let l1, r1 = (min w1.l w2.l), (max w1.r w2.r) in
            match ((jest_przedzialem w1), (jest_przedzialem w2)) with
            | (true, true) ->
                if w1 = rzeczywiste then rzeczywiste
                else if w1.r = infinity then
                    if w2.r = infinity then {l = l1; r = infinity}
                    else aux w2 w1
                else if w1.r >= w2.l then rzeczywiste else {l = w2.l; r = w1.r}
            | (true, false) ->
                assert (not (w1.l > w2.r && w1.r < w2.l));
                if w1.r = infinity then
                    if w1.l <= w2.r then rzeczywiste
                    else {l = (min w1.l w2.l); r = w2.r}
                else
                    if w1.r >= w2.l then rzeczywiste
                    else {l = w2.l; r = (max w1.r w2.r)}
            | (false, true) -> 
                aux w2 w1
            | (false, false) ->
                if r1 >= l1 then rzeczywiste else {l = l1; r = r1}
    in napraw (aux w1 w2)


(* * * * IMPLEMENTACJA KONSTRUKTORÓW * * * *)


(* Użycie konstruktora wartosc_dokladnosc ma tylko sens, gdy p ∈ R+ ∧ x ∈ R,
   albo gdy p = infinity: wtedy wartosc = R, co psuje się m.in. na x = 0 *)
let wartosc_dokladnosc x p = 
    assert (p > 0.);
    assert (x > -.infinity && x < infinity);
    let (l1, r1) = (x *. (1. -. p /. 100.), x *. (1. +. p /. 100.)) in
        if p = infinity then rzeczywiste
        else {l = (min l1 r1); r = (max l1 r1)}

let wartosc_od_do a b =
    assert (a <= b);
    {l = a; r = b}

let wartosc_dokladna x =
    assert (x > -.infinity && x < infinity);
    {l = x; r = x}

let in_wartosc w x =
    if jest_przedzialem w then x >= w.l && x <= w.r else x <= w.r || x >= w.l

let min_wartosc w = 
    if jest_pusty w then nan else
    if jest_przedzialem w then w.l else -.infinity

let max_wartosc w =
    if jest_pusty w then nan else
    if jest_przedzialem w then w.r else infinity

(* przy antyprzedziale średnia wartość nie ma sensu;
   przy braku ograniczenia nan zgodnie ze specyfikacją 
   (brak wartości min lub max - czyli ograniczenia górnego lub dolnego) *)
let sr_wartosc w = 
    if jest_przedzialem w && w.l != -.infinity && w.r != infinity then
    (min_wartosc w +. max_wartosc w) /. 2. else nan


(* * * * IMPLEMENTACJA OPERACJI NA WARTOŚCIACH * * * *)


let plus w1 w2 = 
    (* [a; inf) U (-inf; b] + [c; inf) U (-inf; d] = R
       [a; inf) U (-inf; b] + [c; d] = [a + c; inf) U (-inf; b + d]
       [a; b] + [c; inf) U (-inf; d] = [a + c; inf) U (-inf; d + b] || R
       [a; b] + [c; d] = [a + c; b + d] *)
    if jest_pusty w1 || jest_pusty w2 then pusty else
    if not (jest_przedzialem w1 || jest_przedzialem w2) || 
    (not (jest_przedzialem w1 && jest_przedzialem w2) &&
     (w1.l +. w2.l) <= (w1.r +. w2.r)) then
        rzeczywiste
    else
        {l = w1.l +. w2.l; r = w1.r +. w2.r} 

let przeciwienstwo w = {l = -.w.r; r = -.w.l}

let minus w1 w2 = 
    if jest_pusty w1 || jest_pusty w2 then pusty else
    if not (jest_przedzialem w1 || jest_przedzialem w2) then
        {l = -.infinity; r = infinity}
    else
        plus w1 (przeciwienstwo w2)

(* → https://en.wikipedia.org/wiki/Interval_arithmetic *)
let rec razy w1 w2 =
    if (w1 = pusty || w2 = pusty) then pusty else
    let mozliwosci = [w1.l *. w2.l; w1.l *. w2.r; w1.r *. w2.l; w1.r *. w2.r] in
        if w1 = zero || w2 = zero then zero
        else match ((jest_przedzialem w1), (jest_przedzialem w2)) with
            | (true, true) -> 
                {l = min_in_list mozliwosci; r = max_in_list mozliwosci}
            | (true, false) ->
                let w2a, w2b = rozbij w2 in
                    suma (razy w1 w2a) (razy w1 w2b)
            | (false, _) -> 
                let w1a, w1b = rozbij w1 in
                    suma (razy w1a w2) (razy w1b w2)

let odwrotnosc w =
    if w = zero then pusty else
    if w = rzeczywiste then rzeczywiste
    else let wynik = {l = 1. /. w.r; r = 1. /. w.l} in
        if wynik = pusty then pusty else
        if jest_przedzialem wynik then wynik
        else let a, b = rozbij wynik in
            if a = niesk || a = minus_niesk then b
            else if b = niesk || b = minus_niesk then a
            else wynik

let podzielic w1 w2 =
    assert (w1 != zero && w2 != zero);
    if (w1 = pusty || w2 = pusty) then pusty else
    razy w1 (odwrotnosc w2)
