open Arytmetyka

let a = wartosc_od_do (-1.) 1.            (* <-1, 1> *);;
let b = wartosc_dokladna (-1.)            (* <1, 1> *);;
let c = podzielic b a                     (* (-inf -1> U <1 inf) *);;
let d = plus c a                          (* (-inf, inf) *);;
let e = wartosc_dokladna 0.               (* <0, 0> *);;
let f = razy c e                          (* <0, 0> *);;
let g = razy d e                          (* <0, 0> *);;
let h = wartosc_dokladnosc (-10.) 50.     (* <-15, -5> *);;
let i = podzielic h e                     (* nan, przedzial pusty*);;
let j = wartosc_od_do (-6.) 5.            (* <-6, 5> *);;
let k = razy j j                          (* <-30, 36> *);;
let l = plus a b                          (* <-2, 0> *);;
let m = razy b l                          (* <0, -2> *);;
let n = podzielic l l                     (* <0, inf) *);;
let o = podzielic l m                     (* (-inf, 0) *);;
let p = razy o a                          (* (-inf, inf) *);;
let q = plus n o                          (* (-inf, inf) *);;
let r = minus n n                         (* (-inf, inf) *);;
let s = wartosc_dokladnosc (-0.0001) 100. (* <-0.0002, 0> *);;
let t = razy n s                          (* (-inf, 0) *)
;;

assert ((min_wartosc c, max_wartosc c) = (neg_infinity, infinity));;
assert ((sr_wartosc c) <> (sr_wartosc c));; (*OCaml ma kilka różnych rodzajów nan, nawet jak funkcja zwraca nan to ten test się krzeczy, 30 minut debugowania*)
assert (in_wartosc c 0. = false);;
assert ((in_wartosc c (-1.)) && (in_wartosc c (-100000.)) && (in_wartosc c 1.) && (in_wartosc c 100000.));;
assert ((in_wartosc d 0.) && (in_wartosc d (-1.)) && (in_wartosc d (-100000.)) && (in_wartosc d 1.) && (in_wartosc d 100000.));;
assert ((min_wartosc f, max_wartosc f, sr_wartosc f) = (0., 0., 0.));;
assert ((min_wartosc g, max_wartosc g, sr_wartosc g) = (0., 0., 0.));;
assert ((min_wartosc h, max_wartosc h, sr_wartosc h) = (-15., -5., -10.));;
assert ((compare nan (min_wartosc i), compare nan (sr_wartosc i), compare nan (max_wartosc i)) = (0, 0, 0));;
assert ((min_wartosc k, max_wartosc k, sr_wartosc k) = (-30., 36., 3.));;
assert ((min_wartosc n, max_wartosc n, sr_wartosc n) = (0., infinity, infinity));;
assert ((min_wartosc o, max_wartosc o, sr_wartosc o) = (neg_infinity, 0., neg_infinity));;
assert ((min_wartosc p, max_wartosc p, compare (sr_wartosc p) nan) = (neg_infinity, infinity, 0));;
assert ((min_wartosc q, max_wartosc q, compare (sr_wartosc q) nan) = (neg_infinity, infinity, 0));;
assert ((min_wartosc r, max_wartosc r, compare (sr_wartosc r) nan) = (neg_infinity, infinity, 0));;
assert ((min_wartosc t, max_wartosc t, sr_wartosc t) = (neg_infinity, 0., neg_infinity));;


(* moje testy; wymagają usuniecia asserta z wartosc_od_do *)

let sto = wartosc_dokladnosc 100. 1.;;
let p = wartosc_od_do 1. 2.;;
let p_zero = wartosc_od_do ((-.1.)) 0.;;
let p_znaki = wartosc_od_do (-.0.5) 1.;;
let p_nieogr = wartosc_od_do (-.1.) infinity;;
let a_0 = wartosc_od_do (-.1.) (-.2.);;
let a = wartosc_od_do 100. (-.1.);;
let zero = wartosc_dokladna 0.;;
let r = wartosc_od_do (-.infinity) infinity;;
assert (sto = wartosc_od_do 99. 101.);;

assert (razy a a = wartosc_od_do 1. (-.100.));;

assert (razy p_zero a = r);;
assert (razy p_zero p_zero = wartosc_od_do 0. 1.);;
assert (razy zero r = zero);;
assert (razy p_znaki a = r);;
assert (razy p_znaki p_zero = wartosc_od_do (-.1.) 0.5);;
assert (razy p_znaki p_znaki = p_znaki);;

assert (razy p a = a);;

assert (razy p p_zero = wartosc_od_do (-.2.) 0.);;
assert (razy p p_znaki = wartosc_od_do (-.1.) 2.);;
assert (razy p p = wartosc_od_do 1. 4.);;
assert (razy r a_0 = r);;
assert (razy r p = r);;
assert (podzielic a a = r);;
assert (podzielic a_0 a = r);;
assert (podzielic p_zero a = wartosc_od_do (-.0.01) 1.);;
assert (podzielic zero a = zero);;
assert (podzielic p_znaki a = minus p_znaki (wartosc_dokladna 0.5));;
assert (podzielic p a = wartosc_od_do (-.2.) 0.02);;
assert (podzielic r a = r);;
assert (podzielic p_nieogr a = r);;
assert (podzielic a a_0 = r);;
assert (podzielic a_0 a_0 = r);;
assert (podzielic p_zero a_0 = r);;
assert (podzielic zero a_0 = zero);;
assert (podzielic p_znaki a_0 = r);;
assert (podzielic p a_0 = r);;
assert (podzielic r a_0 = r);;
assert (podzielic p_nieogr a_0 = r);;
assert (podzielic a p_zero = wartosc_od_do 1. (-.100.));;
assert (podzielic a_0 p_zero = r);;
assert (podzielic p_zero p_zero = wartosc_od_do 0. infinity);;
assert (podzielic zero p_zero = zero);;
assert (podzielic p_znaki p_zero = r);;
assert (podzielic p p_zero = wartosc_od_do (-.infinity) (-.1.));;
assert (podzielic a p_znaki = wartosc_od_do 2. (-.1.));;
assert (podzielic a_0 p_znaki = r);;
assert (podzielic a_0 p = r);;
assert (podzielic p p = wartosc_od_do 0.5 2.);;
print_string "Done";;
