Wstęp do programowania funkcyjnego.
Czysto funkcyjnie zapisany program do arytmetyki przedziałów
(tzn. $A + B = C <=> \forall a \in A \and \forall b \in B a + b \in C$,
analogicznie dla innych działań arytmetycznych)